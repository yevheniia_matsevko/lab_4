import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileReader;



import java.util.HashMap;

public class FileWork {
    private static final String FILE_PATH="UserInfo.txt";

    public static void saveInfo(HashMap<String,String> info){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_PATH,false))) {
            for (String key : info.keySet()) {
                String value = info.get(key);
                writer.write(key + ":" + value);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static HashMap<String,String> readInfo(){
        HashMap<String,String> info = new HashMap<>();
        File file = new File(FILE_PATH);
        if (!file.exists()) {
            return new HashMap<>();
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(FILE_PATH))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(":");
                if (parts.length == 2) {
                    String key = parts[0];
                    String value = parts[1];
                    info.put(key, value);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return info;
    }
}
