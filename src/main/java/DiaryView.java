
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Scanner;

public class DiaryView {
    private Scanner scanner;
    private DateTimeFormatter dateFormatter;
    private DiaryService diaryService;


    public DiaryView(){
        scanner = new Scanner(System.in);
        dateFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
        System.out.print("Вітаємо у програмі Ваш особистий щоденник!\n");
        diaryService = new DiaryService(chooseTheDate());
        showMainMenu();
        scanner.close();
    }

    private void showMainMenu() {

        String option="";
        while(!option.equals("0")){
            System.out.println("\nГоловне меню\nВиберіть функцію:\n1 - заповнити щоденник\n2 - переглянути/редагувати дані за певний день\n3 - переглянути/редагувати записи за сьогодні\n0 - завершити роботу\n");
            if(diaryService.contains()){
                System.out.println("-----------------------------------------------------------------\nЗауваження: у ваш вже є записи за цей день.");
                System.out.println("-----------------------------------------------------------------\n");;
            }
            System.out.print("-> ");
            option = scanner.nextLine();
            while(!option.equals("1") && !option.equals("2") && !option.equals("3")&& !option.equals("0")){
                showErrorMessage("Помилка! Такої опції немає! Виберіть іншу");
                System.out.print("-> ");
                option = scanner.nextLine();
            }
            switch (option){
                case "1":
                        option=fillInData();
                    break;
                case "2":
                        option=watchData(null);
                    break;
                case "3":
                        option=watchData(diaryService.getDate());
                    break;
            }
        }
        diaryService.saveData();

    }

    private void showErrorMessage(String text){
        System.out.println("\n!!!!!!  " + text + "  !!!!!!\n");
    }
    private String watchData(DateTime userDate) {
        DateTime dateTime;
        if(userDate==null){
            if(diaryService.isEmpty()){
                showErrorMessage("Помилка! Ваш щоденник ще пустий");
                return "1";
            }
            dateTime = chooseTheDate();
        }else{
            dateTime=userDate;
        }
        if(!diaryService.containsInfoByDate(dateTime)){
            showErrorMessage("Помилка! У вас ще немає записів за цей день!");
            return "1";
        }else{
            System.out.println("\nОсь ваші записи за " + dateTime.toString(dateFormatter));
            System.out.println(diaryService.getInfoByDate(dateTime) + "\n");
        }
        return goBack(dateTime);
    }

    private String fillInData() {
        System.out.print("\nОпишіть, як пройшов ваш день (Щоб закінчити натисніть Enter): \n");
        String text = scanner.nextLine();
        diaryService.setText(text);
        System.out.println("\n-----------------------------------------------------------------");
        System.out.println("Дані успішно збережено!");
        System.out.println("-----------------------------------------------------------------\n");
        return goBack(diaryService.getDate());
    }
    private String goBack(DateTime tempDate){
        System.out.println("Виберіть функцію:\n1 - Повернутись в головне меню\n2 - редагувати дані\n0 - Завершити роботу");
        System.out.print("-> ");
        String option = scanner.nextLine();
        while(!option.equals("1") && !option.equals("2") && !option.equals("0")){
            showErrorMessage("Помилка! Такої опції немає! Виберіть іншу!");
            System.out.print("-> ");
            option = scanner.nextLine();
        }
        if(option.equals("2")){
            option = editData(tempDate);
        }
        return option;
    }

    private String editData(DateTime tempDate) {
        System.out.println("\nВиберіть функцію:\n1 - Повернутись в головне меню\n2 - видалити дані за цей день\n3 - дописати дані за цей день\n4 - перезаписати дані за цей день\n0 - Завершити роботу");
        System.out.print("-> ");
        String option = scanner.nextLine();
        while(!option.equals("1") && !option.equals("2") && !option.equals("0") && !option.equals("3") && !option.equals("4")){
            showErrorMessage("Помилка! Такої опції немає! Виберіть іншу!");
            System.out.print("-> ");
            option = scanner.nextLine();
        }
        switch (option){
            case "2":
                diaryService.deleteInfoByDate(tempDate);
                System.out.println("\n-----------------------------------------------------------------");
                System.out.println("Дані успішно видалено!");
                System.out.println("-----------------------------------------------------------------\n");;

                break;
            case "3":
                System.out.println("\nПродовжуйте опис заданого дня (Щоб закінчити натисніть Enter): ");
                String newText = scanner.nextLine();
                diaryService.updateInfoByDate(tempDate,newText);
                System.out.println("\n-----------------------------------------------------------------");
                System.out.println("Дані успішно оновлено!");
                System.out.println("-----------------------------------------------------------------\n");;


                break;
            case "4":
                System.out.println("\nВведіть новий опис заданого дня (Щоб закінчити натисніть Enter): ");
                String substituteText = scanner.nextLine();
                diaryService.substituteTextByDate(tempDate,substituteText);
                System.out.println("\n-----------------------------------------------------------------");
                System.out.println("Дані успішно оновлено!");
                System.out.println("-----------------------------------------------------------------\n");;

                break;
        }
        return option;
    }

    private DateTime chooseTheDate() {
        System.out.print("\nВведіть дату (dd/MM/yyyy): ");
        String inputDate = scanner.nextLine();
        dateFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
        DateTime date = null;
        while(date==null){
            try{
                date = dateFormatter.parseDateTime(inputDate);
            }catch (IllegalArgumentException ex){
                showErrorMessage("Помилка! Неправильний формат дати!");
                System.out.print("Введіть дату ще раз(dd/MM/yyyy): ");
                inputDate = scanner.nextLine();
            }
        }
        return date;

    }

}
