import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.HashMap;

public class DiaryService {
    private DateTime date;
    private String text;
    private HashMap<String, String> info;
    private DateTimeFormatter dateFormatter;

    public DiaryService(DateTime date){
        dateFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
        this.date=date;
        readData();

    }
    public boolean contains(){
        return info.containsKey(date.toString(dateFormatter));
    }
    private void readData(){
        info=FileWork.readInfo();
    }
    public void saveData(){
        FileWork.saveInfo(info);
    }
    public boolean isEmpty(){
        return info.isEmpty();
    }
    public void deleteInfoByDate(DateTime dateTime){
        if(dateTime.toString(dateFormatter).equals(date.toString(dateFormatter)))
            text="";
        info.remove(dateTime.toString(dateFormatter));
    }
    public void deleteAll(){
        info= new HashMap<>();
    }
    public void updateInfoByDate(DateTime dateTime, String newInfo){
        if(dateTime.toString(dateFormatter).equals(date.toString(dateFormatter)))
            text+=" " + newInfo;
        info.put(dateTime.toString(dateFormatter), info.get(dateTime.toString(dateFormatter))+ " " + newInfo);
    }

    public DateTime getDate() {
        return date;
    }
    public String getInfoByDate(DateTime date){
        return info.get(date.toString(dateFormatter));
    }
    public boolean containsInfoByDate(DateTime date){
        return info.containsKey(date.toString(dateFormatter));
    }

    public void setText(String text) {
        this.text = text;
        info.put(date.toString(dateFormatter), text);
    }
    public void substituteTextByDate(DateTime dateTime, String text){
        if(dateTime.toString(dateFormatter).equals(date.toString(dateFormatter)))
            this.text=text;
        info.put(dateTime.toString(dateFormatter), text);
    }

    public String getText() {
        return text;
    }

}

