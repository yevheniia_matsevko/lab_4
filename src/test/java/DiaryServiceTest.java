import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DiaryServiceTest {
    private DiaryService diaryService;

    @Before
    public void setup() {
        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
        DateTime date = dateFormatter.parseDateTime("01/01/2022");
        diaryService = new DiaryService(date);
    }

    @Test
    public void deleteAllTest() {
        diaryService.deleteAll();
        Assert.assertTrue(diaryService.isEmpty());
    }

    @Test
    public void testDeleteInfoByDate() {
        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
        DateTime date = dateFormatter.parseDateTime("01/01/2022");
        diaryService.deleteInfoByDate(date);
        Assert.assertEquals("", diaryService.getText());
    }

}
